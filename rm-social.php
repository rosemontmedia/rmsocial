<?php
/*
Plugin Name: RM Social
Plugin URI: http://www.rosemontmedia.com
Description: Display social media links at the bottom of blog posts
Author: Craig Freeman
Author URI: craig@rosemontmedia.com
Version: 1.01
*/

add_action('wp_enqueue_scripts', function() {
	// Add appropriate code to header or footer
	// Enqueue script to place fb-root below <body> if not already exists
	wp_register_script( 'rm_social_script', plugins_url('/script.js', __FILE__), array('jquery'), null, true);
	wp_enqueue_script( 'rm_social_script' );
	
	// Stylesheets	
	wp_register_style( 'rm_social_style', plugins_url('/style.css', __FILE__) );
	wp_enqueue_style( 'rm_social_style' );
});
	
add_filter('the_content', function($content) {
	if(is_single() && !this_is('gallery',1)) {
		// Determine which social media icons were specified
		// TO DO
		
		// Tack on appropriate code to content of single blog post
		$str = '
			<div class="rm-sharingBtns">
				<div class="fb-like" data-href="'.get_permalink().'" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
				<a style="margin-top:3px;" href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				<g:plusone size="medium" annotation="none"></g:plusone>
				<script type="text/javascript">
					(function() {
						var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
					    po.src = "https://apis.google.com/js/plusone.js";
					    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
					})();
				</script>
				<script src="//platform.linkedin.com/in.js" type="text/javascript">
				  lang: en_US
				</script>
				<script type="IN/Share"></script>
				
				
			</div>';
		
		return $content . $str;
	} else {
		return $content;
	}
});